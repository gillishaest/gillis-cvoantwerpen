-- If database does not exist, create the database
CREATE DATABASE IF NOT EXISTS Competition;
USE `Competition`;
DROP TABLE IF EXISTS 'Player';
DROP TABLE IF EXISTS 'Team';
DROP TABLE IF EXISTS 'League';
DROP TABLE IF EXISTS 'Game';
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `Player`;
CREATE TABLE `Player` (
	`Id` INT UNSIGNED NOT NULL AUTO_INCREMENT, 
	CONSTRAINT PRIMARY KEY(Id),
	`FirstName` NVARCHAR (50) NOT NULL,
	`LastName` NVARCHAR (120) NOT NULL,
	`Email` NVARCHAR (255) NULL,
	`Address1` NVARCHAR (255) NULL,
	`Address2` NVARCHAR (255) NULL,
	`PostalCode` VARCHAR (20) NULL,
	`City` NVARCHAR (80) NULL,
	`Country` NVARCHAR (40) NULL,
	`Phone` VARCHAR (25) NULL,
	`Birthday` DATETIME NULL,
	`TeamId` INT UNSIGNED NULL, 
	CONSTRAINT fk_PlayerTeamId FOREIGN KEY (`TeamId`) REFERENCES `Team` (`Id`));

DROP TABLE IF EXISTS `Liga`;
CREATE TABLE `Liga` (
`Id` INT UNSIGNED NOT NULL AUTO_INCREMENT, 
	CONSTRAINT PRIMARY KEY(Id),
	`Name` NVARCHAR (50) NOT NULL,
	`Year` CHAR (4) NOT NULL,
	`IsInPlanning` BIT NULL);
	
DROP TABLE IF EXISTS `Team`;
CREATE TABLE `Team` (
	`Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	CONSTRAINT uc_Team_Name UNIQUE (Name)
	`Name` NVARCHAR (50) NOT NULL,
	`Location` NVARCHAR (50) NOT NULL,
	`Score` NVARCHAR (50) NOT NULL);

DROP TABLE IF EXISTS `Game`;
CREATE TABLE `Game` (
	`Id` INT UNSIGNEDNOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	`Date` DATETIME NULL,
	`Status` NVARCHAR (120) NOT NULL,
	`ScoreHome` NVARCHAR (120) NOT NULL,
	`ScoreVisitors` NVARCHAR (120) NOT NULL,
	`TeamHomeId` INT UNSIGNED NULL,
	`TeamVisitorId` INT UNSIGNED NULL,
	`LigaId` INT UNSIGNED NULL,
	CONSTRAINT fk_GameTeamHomeId FOREIGN KEY (`TeamHomeId`) REFERENCES `Team` (`Id`),
	CONSTRAINT fk_GameTeamVisitorId FOREIGN KEY (`TeamVisitorId`) REFERENCES `Team` (`Id`),
	CONSTRAINT fk_GameLigaId FOREIGN KEY (`LigaId`) REFERENCES `Liga` (`Id`));

-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 1;
