use Cursist5;

DROP TABLE IF EXISTS 'Player'
CREATE TABLE 'Player' (
	'Id' INT NOT NULL AUTO_INCREMENT, CONSTRAINT PRIMARY KEY(Id), 
    'TeamId' INT NULL, CONSTRAINT fk_PlayerTeamId FOREIGN KEY('TeamId') REF..
    'FirstName' NVARCHAR(50) NOT NULL,
    'LastName' NVARCHAR(120) NOT NULL,
    'Email'NVARCHAR(255) NOT NULL,
    'Address1' NVARCHAR(255) NOT NULL,
    'Address2' NVARCHAR(255) NOT NULL,
    'PostalCode' VARCHAR(20) NOT NULL,
    'City' NVARCHAR(80) NOT NULL,
    'Country' NVARCHAR(40) NOT NULL,
    'Phone' VARCHAR(25) NOT NULL,
    'Birthday' DATETIME NULL
);

DROP TABLE IF EXISTS 'Team'
CREATE TABLE 'Team' {
    
    
};

DROP TABLE IF EXISTS 'League'
CREATE TABLE 'League' {
    
};

DROP TABLE IF EXISTS 'Game'
CREATE TABLE 'Game'  {
    'Id'
    'Starts' DATETIME NULL,
    'Data'
    'Status'
    'ScoreHome'
    'ScoreVisitors'
    'TeamHomeId'
    'TeamVisitorsId'
};