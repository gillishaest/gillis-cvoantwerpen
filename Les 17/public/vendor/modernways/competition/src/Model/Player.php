<?php
namespace ModernWays\Competition\Model;

class Player 
{
    private $lastName;
    private $firstName;
    private $address;
    private $shirtNumber;
    
    public function setLastName($lastName) 
    {
        //je gebruikt een setter als je wilt controleren
        //of de waarde die je wilte toekennen aan een veld
        //een geschikte waarde is 
        //indien dat niet nodig is, maak je het veld public
        if (empty($lastName)) {
            return false;
        } else {
            $this->lastName = $lastName; 
            return true;
        }
     }
    
    public function getLastName() 
    {
        return $this->lastName;
    }
    
    public function setFirstName($firstName) 
    {
        //je gebruikt een setter als je wilt controleren
        //of de waarde die je wilte toekennen aan een veld
        //een geschikte waarde is 
        //indien dat niet nodig is, maak je het veld public
        if (empty($firstName)) {
            return false;
        } else {
            $this->firstName = $firstName; 
            return true;
        }
     }
    
    public function getFirstName() 
    {
        return $this->firstName;
    }
    
    public function setAddress($address) 
    {
        //je gebruikt een setter als je wilt controleren
        //of de waarde die je wilte toekennen aan een veld
        //een geschikte waarde is 
        //indien dat niet nodig is, maak je het veld public
        if (empty($address)) {
            return false;
        } else {
            $this->address = $address; 
            return true;
        }
     }
    
    public function getAddress() 
    {
        return $this->address;
    }
    
    public function setShirtNumber($shirtNumber) 
    {
        //je gebruikt een setter als je wilt controleren
        //of de waarde die je wilte toekennen aan een veld
        //een geschikte waarde is 
        //indien dat niet nodig is, maak je het veld public
        if (empty($shirtNumber)) {
            return false;
        } else {
            $this->shirtNumber = $shirtNumber; 
            return true;
        }
     }
    
    public function getShirtNumber() 
    {
        return $this->shirtNumber;
    }    
}