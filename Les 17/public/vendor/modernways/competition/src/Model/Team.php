<?php
namespace ModernWays\Competition\Model;

class Team
{
    private $name;
    private $location;
    private $points;
   
    
    //je gebruikt een setter als je wilt controleren
    //of waarde die je wilt toekennen aan een veld
    //een geschikte waarde is
    //indien dat niet nodig is, maak je het veld public
    public function setName($name) 
    {
        if (empty ($name))
        {
            return false;
        } else {
            $this->name = $name;
            return true;
        }
    }
    
    public function getName() 
    {
        return $this->name;
    }
    
    public function setLocation($location) 
    {
        if (empty ($location))
        {
            return false;
        } else {
            $this->location = $location;
            return true;
        }
    }
    
    public function getLocation() 
    {
        return $this->location;
    }
    
    public function setPoints($points) 
    {
        if (empty ($points))
        {
            return false;
        } else {
            $this->points = $points;
            return true;
        }
    }
    
    public function getPoints() 
    {
        return $this->points;
    }
}