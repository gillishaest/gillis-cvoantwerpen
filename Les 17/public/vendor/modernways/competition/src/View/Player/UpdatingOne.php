<header>
    <h1>Updating Player</h1>
    <a>Update</a> <a>Cancel</a>
</header>
<form>
    <fieldset>
        <legend>Player</legend>
        <div>
            <label>LastName:</label>
            <input name="LastName" id="LastName" value="<?php echo $player->getLastName();?>" />
        </div>
        <div>
            <label>FirstName:</label>
            <input name="FirstName" id="FirstName" value="<?php echo $player->getFirstName();?>" />
        </div>
        <div>
            <label>Address:</label>
            <input name="Address" id="Address" value="<?php echo $player->getAddress();?>" />
        </div>
        <div>
            <label>BackNumber:</label>
            <input name="BackNumber" id="BackNumber" value="<?php echo $player->getShirtNumber();?>" />
        </div>
    </fieldset>
</form>
    