<header>
    <h1>Player</h1>
    <a>Update</a> <a>Delete</a> <a>Insert</a>
</header>
<form>
    <fieldset>
        <legend>Player</legend>
        <div>
            <label>Lastname</label>
            <input name="Lastname" id="Lastname" value="<?php echo $speler->getLastname();?>" readonly/>
        </div>
        <div>
            <label>Voornaam</label>
            <input name="Firstname" id="Firstname" value="<?php echo $speler->getVoornaam();?>" readonly/>
        </div>
        <div>
            <label>Adres</label>
            <input name="Address" id="Address" value="<?php echo $speler->getAdres();?>" readonly/>
        </div>
        <div>
            <label>Rugnummer</label>
            <input name="Backnumber" id="Backnumber" value="<?php echo $speler->getRugnummer();?>" readonly/>
        </div>
    </fieldset>
</form>