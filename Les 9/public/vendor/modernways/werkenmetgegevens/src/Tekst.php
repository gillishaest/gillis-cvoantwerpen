<?php
namespace ModernWays\WerkenMetGegevens;

    class Tekst
    {
        public function declareText(){
            $tekst =  'Ik zou graag een pake friet met mayonaise willen.' . '<br>';
            print $tekst;
            $tekst = 'fricandel' . '<br>';
            print $tekst;
            $tekst = '€ 5.50' . '<br>';
            print $tekst;
            $tekst = '"Ik ben aan het werken," zie ze.' . '<br>';
            print $tekst;
        }
        
        public function concatenateText(){
            $groet = 'Een goede morgen';
            $aan = 'Jan';
            $wens = 'toegewenst.';
            //$zin = $groet . ' ' . $$aan . '  ' . $wens;
            $zin = "$groet $aan $wens" . '<br>';
            echo $zin;
        }
        
        public function lengthText(){
            // $_POST['zipcode'] holds the value of the submitted form parameter "zipcode"
            $zipcode = trim($_POST['zipcode']);
            // Now $zipcode holds that value, with any leading or trailing spaces removed
            $zip_length = strlen($zipcode);


            // Complain if the zip code is not 4 characters long
            if ($zip_length != 4) {
            print "Een postcode bestaat uit maximum 4 karakter." . '<br>';
            }

            //combined strlen & trim
            if (strlen(trim($_POST['zipcode'])) != 4) {
                print 'Een postcode bestaat uit maximum 4 karakter.' . '<br>';
            }
        }
        
        public function printfText(){
            $zip = '6520';
	        $month = 2;
	        $day = 6;
	        $year = 2018;
	        echo sprintf("Postcode is %05d en de datum is %02d/%02d/%d" . '<br>', $zip, $month, $day, $year);
        }
        
        public function signModifierText(){
            $min = -4;
            $max = 32;
            echo sprintf("Dit jaar was de minimum temperatuur %+d en de maximum %+d graden Celsius." . '<br>', $min, $max);
        }
        
        public function upperLowercaseText(){
            echo strtolower('Victoires, Café, Brasserie, bAR' . '<br>');
            echo strtoupper('Victoires, Café, Brasserie, bAR' . '<br>');
            echo ucwords('bob dylan' . '<br>');
            echo ucfirst('bob dylan' . '<br>');
            echo lcfirst('BOB DYLAN' . '<br>');
        }
        
        public function substrText(){
            echo 'Card: XX' . '<br>';
            echo substr($_POST['card' . '<br>'],-4,4);
        }
        
        public function replaceText(){
            $cssClass = 'lunch';
            $html = '<span class = "{class}">Friet met mayonaise<span><br><span class = "{class}">Frikandel met ui</ span> ';
            echo str_replace ('{class}' . '<br>', $cssClass, $html);
        }
    }
?>