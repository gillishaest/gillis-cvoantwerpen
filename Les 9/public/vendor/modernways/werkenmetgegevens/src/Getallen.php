<?php
namespace ModernWays\WerkenMetGegevens;

    class Getallen
    {
        public function declareNumbers(){
            $integer = 56;
            $decimal = 56.3;
            $decimal2 = 56.30;
            $float = 0.774422;
            $float2 = 16777.216;
            $null = 0;
            $negative = -213;
            $integer2 = 1298317;
            $negativeInteger = -9912111;
            $negativeFloat = -12.52222;
            $decimal3 = 0.00;
            
            echo $integer * $integer . '<br>';
            echo $integer / $integer . '<br>';
            echo $integer - $integer . '<br>';
            echo $integer + $integer . '<br>';
            //echo $integer ** $integer;
            echo pow($integer, $integer) . '<br>';
            echo $integer % $integer . '<br>';
        }
    }
?>