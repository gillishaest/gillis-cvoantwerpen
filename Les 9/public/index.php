<?php
include __DIR__ . "/vendor/autoload.php";

//instantie creëren van de getallen klasse
$getallen = new \ModernWays\WerkenMetGegevens\Getallen;
$boolean = new \ModernWays\WerkenMetGegevens\Boolean;
$tekst = new \ModernWays\WerkenMetGegevens\Tekst;

//informatie op de pagina weergeven
echo "NUMMERS <br>";
$getallen->declareNumbers();
echo "<br>";
echo "BOOLEAN <br>";
$boolean->boolean();
echo "<br>";
echo "TEKST <br>";
$tekst->declareText();
$tekst->concatenateText();
$tekst->lengthText();
$tekst->printfText();
$tekst->signModifierText();
$tekst->substrText();
$tekst->replaceText();
?>

<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Werken Met Gegevens</title>
    <link rel="stylesheet" href="css/app.css" type="text/css" />
</head>
<body>
    
</body>
</html>
