<?php
include __DIR__ . "/vendor/autoload.php";

//instantie creëren van de getallen klasse
$getallen = new \ModernWays\Berekeningen\Getallen;
$getallen->declareNumbers();
$getallen->boolean();
?>

<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Beheer Competitie</title>
    <link rel="stylesheet" href="css/app.css" type="text/css" />
</head>
<body>
    
</body>
</html>
