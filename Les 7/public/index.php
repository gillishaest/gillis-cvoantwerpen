<?php
//globale variabele bevat info over http request
print_r($_REQUEST);
//ervoor zorgen dat je altijd een waarde in $useCase staan hebt
$useCase = 'Admin/Index';
//Isset is een methode om na te gaan of bepaald element in de associatieve array staat.
if (isset($_REQUEST['uc'])){
    $useCase = $_REQUEST['uc'];
}
echo 'use case in de querystring ' . $useCase;
switch ($useCase){
    case 'Admin/index':
        $path='/Les 7/public/vendor/modernways/competitie/src/View/Admin/Index.php';
        break;
    case 'Player/Editing':
        $path='/Les 7/public/vendor/modernways/competitie/src/View/Speler/Editing.php';
        break;
    case 'Team/Editing':
        $path='/Les 7/public/vendor/modernways/competitie/src/View/Team/Editing.php';
        break;
    case 'Wedstrijd/Editing':
        $path='/Les 7/public/vendor/modernways/competitie/src/View/Wedstrijd/Editing.php';
        break; 
    case 'Liga/Editing':
        $path='/Les 7/public/vendor/modernways/competitie/src/View/Liga/Editing.php';
        break;
}
?>

<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Beheer Competitie</title>
    <link rel="stylesheet" href="css/app.css" type="text/css" />
</head>
<body>
    <?php include($path); ?>
</body>
</html>
