<?php
namespace ModernWays\Competitie\Model;

class Team
{
    private $naam;
    private $locatie;
    private $aantalpunten;
   
    
    //je gebruikt een setter als je wilt controleren
    //of waarde die je wilt toekennen aan een veld
    //een geschikte waarde is
    //indien dat niet nodig is, maak je het veld public
    public function setNaam($naam) 
    {
        if (empty ($naam))
        {
            return false;
        } else {
            $this->naam = $naam;
            return true;
        }
    }
    
    public function getNaam() 
    {
        return $this->naam;
    }
    
    public function setLocatie($locatie) 
    {
        if (empty ($locatie))
        {
            return false;
        } else {
            $this->locatie = $locatie;
            return true;
        }
    }
    
    public function getLocatie() 
    {
        return $this->locatie;
    }
    
    public function setAantalPunten($aantalpunten) 
    {
        if (empty ($aantalpunten))
        {
            return false;
        } else {
            $this->aantalpunten = $aantalpunten;
            return true;
        }
    }
    
    public function getAantalPunten() 
    {
        return $this->aantalpunten;
    }
}