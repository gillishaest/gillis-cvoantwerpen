<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 10/09/2018
 * Time: 20:24
 */
include('Speler.php');
include('Liga.php');
include('Team.php');
include('Wedstrijd.php');

// instantie creëren van de speler klasse
$speler = new \Competitie\Speler;
$speler->setFamilienaam('Haest');
$liga = new \Competitie\Liga;
$liga->setNaam('België');
$team = new \Competitie\Team;
$team->setNaam('De toppers');
$wedstrijd = new \Competitie\Wedstrijd;
$wedstrijd->setDatum(date('d/m/Y'));

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Leren werken met PHP les 4</title>
    <form>
        <fieldset>
            <legend>Speler</legend>
            <div>
            <label>Familienaam: </label>
            <input value="<?php echo $speler->getFamilienaam(); ?>">
            </div>
        </fieldset>
        <fieldset>
            <legend>Liga</legend>
            <div>
            <label>Naam: </label>
            <input value="<?php echo $liga->getNaam(); ?>">
            </div>
        </fieldset>
        <fieldset>
            <legend>Team</legend>
            <div>
            <label>Naam: </label>
            <input value="<?php echo $team->getNaam(); ?>">
            </div>
        </fieldset>
        <fieldset>
            <legend>Wedstrijd</legend>
            <div>
            <label>Datum: </label>
            <input value="<?php echo $wedstrijd->getDatum(); ?>">
            </div>
        </fieldset>
    </form>
</head>
<body>
    <h1>Leren werken met PHP</h1>
    <?php include('footer.php'); ?>
</body>
</html>
