<?php
namespace Competitie;

class Speler 
{
    private $familienaam;
    private $voornaam;
    private $adres;
    private $rugnummer;
    
    public function setFamilienaam($familienaam) 
    {
        
        //je gebruikt een setter als je wilt controleren
        //of de waarde die je wilte toekennen aan een veld
        //een geschikte waarde is 
        //indien dat niet nodig is, maak je het veld public
        if (empty($familienaam)) {
            return false;
        } else {
            $this->familienaam = $familienaam; 
            return true;
        }
     }
    
    public function getFamilienaam() 
    {
        return $this->familienaam;
    }
}