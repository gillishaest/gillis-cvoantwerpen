<?php
namespace Competitie;

class Liga
{
    private $naam;
    private $jaargang;
    private $isInPlanning;
  
    
    public function setNaam($naam) 
    {
        if (empty ($naam))
        {
            return false;
        } else {
            $this->naam = $naam;
            return true;
        }
    }
    
    public function getNaam() 
    {
        return $this->naam;
    }
    
    public function setJaargang($jaargang) 
    {
        if (empty ($jaargang))
        {
            return false;
        } else {
            $this->jaargang = $jaargang;
            return true;
        }
    }
    
    public function getJaargang() 
    {
        return $this->jaargang;
    }
    
    public function setisInPlanning($isInPlanning) 
    {
        if (empty ($isInPlanning))
        {
            return false;
        } else {
            $this->isInPlanning = $isInPlanning;
            return true;
        }
    }
    
    public function getisInPlanning() 
    {
        return $this->isInPlanning;
    }
}