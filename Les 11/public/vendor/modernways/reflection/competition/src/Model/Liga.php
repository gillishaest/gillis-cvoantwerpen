<?php
namespace ModernWays\Competition\Model;

class Liga
{
    private $name;
    private $year;
    private $isInPlanning;
  
    
    public function setName($name) 
    {
        if (empty ($name))
        {
            return false;
        } else {
            $this->name = $name;
            return true;
        }
    }
    
    public function getName() 
    {
        return $this->name;
    }
    
    public function setYear($year) 
    {
        if (empty ($year))
        {
            return false;
        } else {
            $this->year = $year;
            return true;
        }
    }
    
    public function getYear() 
    {
        return $this->year;
    }
    
    public function setisInPlanning($isInPlanning) 
    {
        if (empty ($isInPlanning))
        {
            return false;
        } else {
            $this->isInPlanning = $isInPlanning;
            return true;
        }
    }
    
    public function getisInPlanning() 
    {
        return $this->isInPlanning;
    }
}