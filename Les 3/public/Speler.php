<?php
namespace Competitie;

class Speler 
{
    private $familienaam;
    private $voornaam;
    private $adres;
    private $rugnummer;
    
    public function setFamilienaam($familienaam) 
    {
        
        //je gebruikt een setter als je wilt controleren
        //of de waarde die je wilte toekennen aan een veld
        //een geschikte waarde is 
        //indien dat niet nodig is, maak je het veld public
        if (empty($familienaam)) {
            return false;
        } else {
            $this->familienaam = $familienaam; 
            return true;
        }
     }
    
    public function getFamilienaam() 
    {
        return $this->familienaam;
    }
    
    public function setVoornaam($voornaam) 
    {
        if (empty($voornaam)) {
            return false;
        } else {
            $this->voornaam = $voornaam; 
            return true;
        }
     }
    
    public function getVoornaam() 
    {
        return $this->voornaam;
    }
    
    public function setAdres($adres) 
    {
        if (empty($adres)) {
            return false;
        } else {
            $this->adres = $adres; 
            return true;
        }
     }
    
    public function getAdres() 
    {
        return $this->adres;
    }
    
    public function setRugnummer($rugnummer) 
    {
        if (empty($rugnummer)) {
            return false;
        } else {
            $this->rugnummer = $rugnummer; 
            return true;
        }
     }
    
    public function getRugnummer() 
    {
        return $this->rugnummer;
    }
}
