<?php

/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 10/09/2018
 * Time: 20:24
 */

include('Speler.php');

// instantie creëren van de speler klasse
$speler = new \Competitie\Speler;

$speler->setFamilienaam('Haest');
$speler->setVoornaam('Gillis');
$speler->setAdres('Ranst');
$speler->setRugnummer('1');
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Leren werken met PHP les 3</title>
    <form>
        <fieldset>
            <legend>Speler</legend>
            <div>
            <label>Familienaam: </label>
            <input value="<?php echo $speler->getFamilienaam(); ?>">
            </div>
            <div>
            <label>Voornaam: </label>
            <input value="<?php echo $speler->getVoornaam(); ?>">
            </div>
            <div>
            <label>Adres: </label>
            <input value="<?php echo $speler->getAdres(); ?>">
            </div>
            <div>
            <label>Rugnummer: </label>
            <input value="<?php echo $speler->getRugnummer(); ?>">
            </div>
        </fieldset>
    </form>
</head>
<body>
    <h1>Leren werken met PHP</h1>
    <?php include('footer.php'); ?>
</body>
</html>
