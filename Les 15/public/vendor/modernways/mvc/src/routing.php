<?php
/**
* Created by PhpStorm.
* User: jefinghelbrecht
* Date: 24/10/18
* Time: 20:11
*/
include __DIR__ . '/vendor/autoload.php';
\ModernWays\MVC\Routing::setRouteFromUrl($_SERVER['SCRIPT_NAME'], $_SERVER['REDIRECT_URL']);
\ModernWays\MVC\Routing::setRoute($_SERVER['PATH_INFO']);
$defaultRoute = 'ModernWays\Competition/Admin/Index';
\ModernWays\MVC\Routing::init($defaultRoute);

switch (\ModernWays\MVC\Routing::getEntity())
{
    case 'player' : {
        switch (\ModernWays\MVC\Routing::getAction()) {
            case 'create' :
                echo 'je gaat een speler aanmaken';
                break;
            case 'readingone' :
                $player = new \ModernWays\Competition\Model\Player();
                $player->setLastName('Inghelbrecht');
                $player->setFirstName('Jef');
                $player->setAddress('Monaco');
                $player->setShirtNumber('1');
                $view = __DIR__ . '/vendor/modernways/competition/src/View/Player/ReadingOne.php';
                break;
            case 'updatingone' :
                // in het echt lezen we de gegevens uit de database in
                // connectie met mysql
                $player = new \ModernWays\Competition\Model\Player();
                $player->setLastName('Inghelbrecht');
                $player->setFirstName('Jef');
                $player->setAddress('Monaco');
                $player->setShirtNumber('1');
                $view = __DIR__ . '/vendor/modernways/competition/src/View/Player/UpdatingOne.php';
                break;
            case 'creatingone' :
                $view = __DIR__ . '/vendor/modernways/competition/src/View/Player/CreatingOne.php';
                break;
            case 'editing' :
                $view = __DIR__ . '/vendor/modernways/competition/src/View/Player/Editing.php';
                break;
            case 'delete' :
                echo 'je gaat een speler deleten';
                break;
        }
    }
    case 'liga' : {
        switch ($action) {
            case 'create' :
                echo 'je gaat een speler aanmaken';
                break;
            case 'read' :
                echo 'je gaat een speler inlezen';
                break;
            case 'update' :
                echo 'je gaat een speler updaten';
                break;
            case 'delete' :
                echo 'je gaat een speler deleten';
                break;
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jef's Competitie</title>
</head>
<body>
<h1>Is de pagina die wordt opgeroepen als de bezoeker op onze Competition pagina komt</h1>
<pre id="feedback">
    <?php echo \ModernWays\MVC\Routing::getInfo(); ?>
    <?php echo var_dump($_SERVER);?>
</pre>
</body>
</html>