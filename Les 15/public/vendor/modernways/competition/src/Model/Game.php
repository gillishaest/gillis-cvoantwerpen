<?php
namespace ModernWays\Competition\Model;

class Game
{
    private $date;
    private $time;
    private $status;
    private $scoreHome;
    private $scoreVisitors;
    
    //je gebruikt een setter als je wilt controleren of waarde 
    //die je wilt toekennen aan een veld een geschikte waarde is
    //indien dat niet nodig is, maak je het veld public
    public function setDate($date) 
    {
        if (empty ($date))
        {
            return false;
        } else {
            $this->date = $date;
            return true;
        }
    }
    
    public function getDate() 
    {
        return $this->date;
    }
    
    public function setTime($time) 
    {
        if (empty ($time))
        {
            return false;
        } else {
            $this->time = $time;
            return true;
        }
    }
    
    public function getTime() 
    {
        return $this->time;
    }
    
    public function setStatus($status) 
    {
        if (empty ($status))
        {
            return false;
        } else {
            $this->status = $status;
            return true;
        }
    }
    
    public function getStatus() 
    {
        return $this->status;
    }
    
    public function setScoreHome($scoreHome) 
    {
        if (empty ($scoreHome))
        {
            return false;
        } else {
            $this->scoreHome = $scoreHome;
            return true;
        }
    }
    
    public function getScoreHome() 
    {
        return $this->scoreHome;
    }
    
    public function setScoreVisitors($scoreVisitors) 
    {
        if (empty ($scoreVisitors))
        {
            return false;
        } else {
            $this->scoreVisitors = $scoreVisitors;
            return true;
        }
    }
    
    public function getScoreVisitors() 
    {
        return $this->scoreVisitors;
    }
}   
